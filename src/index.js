/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/first */
/** @flow */
require('@babel/polyfill')
require('@babel/register')


import Koa from 'koa'
import koaStatic from 'koa-static'
import Router from 'koa-router'
import path from 'path'
import type { KoaContext } from 'koa-flow-declarations/KoaContext'
import { matchRoutes } from 'react-router-config'

import renderer from './helpers/renderer'
import Routes from './client/Routes'
import configureStore from './client/store'
import rootSaga from './client/actions'

const PORT:string = '3000'


const app = new Koa()
const router = new Router()

app.use(koaStatic(`${path.resolve()}/public`))
router.get('*', async (ctx: KoaContext) => {
  const store = configureStore()
  store.runSaga(rootSaga)
  const promises = matchRoutes(Routes, ctx.request.path)
    .reduce((prom, { route }) => {
      if (route.loadData) {
        prom.push(route.loadData(store))
      }
      return prom
    }, [])
  await Promise.all(promises)
  const html = renderer(ctx.request, store)
  ctx.body = html
})


app.use(router.routes())
  .use(router.allowedMethods())


app.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`On Port: ${PORT}`)
})
