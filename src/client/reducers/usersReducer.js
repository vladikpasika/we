import { FETCH_USERS } from '../actions/constants'

export default (state = [], action) => {
  switch (action.type) {
    case FETCH_USERS.SUCCESS:
      return action.payload.data;
    default:
      return state
  }
}
