/** @flow */
// import React from 'react'
import UserListPage from './pages/UserListPage'
import HomePage from './pages/HomePage';

export default [
  {
    ...HomePage,
    path: '/',
    exact: true
  },
  {
    ...UserListPage,
    path: '/users'
  }
]
