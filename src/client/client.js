/* eslint-disable import/first */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/prefer-default-export */
/** @flow */
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { renderRoutes } from 'react-router-config'
import configureStore from './store'
import rootSaga from './actions'

import Routes from './Routes'

const rootElement = document.querySelector('#root')
const store = configureStore(window.INITIAL_STATE)
store.runSaga(rootSaga)

if (rootElement) {
  ReactDOM.hydrate(
    <Provider store={store}>
      <BrowserRouter>
        <div>{renderRoutes(Routes)}</div>
      </BrowserRouter>
    </Provider>, rootElement
  )
}
