import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import sagaMonitor from '@redux-saga/simple-saga-monitor'
import { middleware as thunkMiddleware } from 'redux-saga-thunk'


import reducers from '../reducers'

export default (initialState = {}) => {
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
  const store = createStore(reducers, initialState,
    composeWithDevTools(applyMiddleware(thunkMiddleware, sagaMiddleware)))

  store.runSaga = sagaMiddleware.run
  return store
}
