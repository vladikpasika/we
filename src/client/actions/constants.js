/* eslint-disable import/prefer-default-export */
const constantsBuilder = constanta => ({
  SUCCESS: `${constanta}.SUCCESS`,
  ERROR: `${constanta}.ERROR`,
  REQUEST: `${constanta}.REQUEST`
})

export const FETCH_USERS = constantsBuilder('FETCH_USERS')
