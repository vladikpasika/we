import axios from 'axios'
import { put, takeLatest, all } from 'redux-saga/effects'
import { FETCH_USERS } from './constants'

function* fetchUsers({ meta }) {
  const response = yield axios.get('http://react-ssr-api.herokuapp.com/users/xss')
  yield put({ type: FETCH_USERS.SUCCESS, payload: response, meta });
}

function* actionWatcher() {
  yield takeLatest(FETCH_USERS.REQUEST, fetchUsers)
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
