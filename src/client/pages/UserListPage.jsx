import React, { Component } from 'react'
import { connect } from 'react-redux'

import { FETCH_USERS } from '../actions/constants'

class UserList extends Component {
  componentDidMount() {
    this.props.fetchUsers()
  }

  render() {
    const { users } = this.props
    return (
      <div>
        Here is a big list of users!
        <ul>
          {users.map(person => (
            <li key={person.id}>{person.name}</li>
          ))}
        </ul>
      </div>
    )
  }
}
const mapStateToProps = state => ({ users: state.users })

const loadData = store => store.dispatch({ type: FETCH_USERS.REQUEST, meta: { thunk: true } })

const mapDispathToProps = dispatch => (
  { fetchUsers: () => dispatch({ type: FETCH_USERS.REQUEST, meta: { thunk: true } }) }
)

export default {
  loadData,
  component: connect(mapStateToProps, mapDispathToProps)(UserList)
}
