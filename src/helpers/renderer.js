/** @flow */
import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { renderRoutes } from 'react-router-config'
import serialize from 'serialize-javascript'

import Routes from '../client/Routes'


export default (req: any, store: any) => {
  const content = renderToString(
    <Provider store={store}>
      <StaticRouter context={{}} location={req.path}>
        <div>{renderRoutes(Routes)}</div>
      </StaticRouter>
    </Provider>,
  )
  return (`<html>
      <head></head>
      <script>window.INITIAL_STATE = ${serialize(store.getState())}</script>
      <script async src="bundle.js"></script>

      <body>
        <div id="root">${content}<div>
      </body>
    </html>`)
}
